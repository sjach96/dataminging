import pandas as pd
import numpy as np
import sklearn.cluster
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import dendrogram, linkage


class HierarchicalClustering:

    def __init__(self, data_frame, **kwargs):
        if isinstance(data_frame, dict):
            self._data_frame = pd.DataFrame(data_frame)
        elif isinstance(data_frame, pd.DataFrame):
            self._data_frame = data_frame
        else:
            raise Exception("Unknown type! Expected DataFrame or dict, get: {}".format(type(data_frame)))

        self.hierarchical_cluster = sklearn.cluster.AgglomerativeClustering(**kwargs)

        self.target = None
        self.train = None
        self.cluster = None

    def set_target(self, target):
        self.target = target
        self.train = np.array(self._data_frame.drop([target], 1))
        return self

    def evaluate(self):
        self.hierarchical_cluster.fit(self.train)
        return self

    def get_prediction(self):
        self.cluster = self.hierarchical_cluster.fit_predict(self._data_frame.drop([self.target], 1))
        return self.cluster

    def plot(self, filename=None):
        plt.figure(figsize=(10, 7))
        X = self._data_frame.iloc[:, 1:-1].values
        link = linkage(X, 'ward')
        dendrogram(link,
                   orientation='top',
                   distance_sort='descending',
                   show_leaf_counts=True)
        # plt.show()

