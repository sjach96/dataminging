import sys
import operator
import collections
import csv
import pandas
import matplotlib.pyplot as plt
import cr_tree
import kmeans
import hierarchical_clusters


def read_csv(file, rows_count='all', first_columns_name=True):
    all_rows = False
    if isinstance(rows_count, str) and rows_count == 'all':
        all_rows = True
    elif isinstance(rows_count, str) and rows_count != 'all':
        raise Exception("Unrecognized argument meaning: {}!".format(rows_count))
    elif not isinstance(rows_count, int):
        raise Exception("Expected type is str or int - get {}".format(type(rows_count)))

    result = []
    headers = []
    with open(file) as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0 and first_columns_name:
                headers.extend(row)
            else:
                result.append(row)
            line_count += 1
            if not all_rows:
                if line_count > rows_count:
                    break
    return result, headers


def get_dict_from_data_and_headers(data, headers):
    result = dict()
    tmp_dict = []
    for index, header in enumerate(headers):
        result[header] = []
        tmp_dict.append(header)

    for row in data:
        for index, value in enumerate(row):
            try:
                result[tmp_dict[index]].append(float(value) if '.' in value else int(value))
            except ValueError:
                result[tmp_dict[index]].append(value.strip(' '))

    return result


def draw_linear(data, name=None, x_axis=None):
    plt.plot(data)
    plt.ylabel(name)
    plt.show()


def draw_hist(data, name=None, x_axis=None, filename=None):
    commutes = pandas.Series(data)
    commutes.plot.hist(grid=True, bins=11, rwidth=0.9,
                       color='#607c8e')
    plt.ylabel('Counts')
    plt.xlabel(name)
    plt.grid(axis='y', alpha=0.75)
    plt.savefig('/home/sjach/Projects/June2019/dataminging/raport/img/{}'.format(filename), format='pgf')
    plt.show()


def get_assignments(dict_data, cluster_column_name, target_column_name):
    to_cluster = dict()
    for index, loc in enumerate(dict_data[target_column_name]):
        try:
            to_cluster[loc].append(dict_data[cluster_column_name][index])
        except KeyError:
            to_cluster[loc] = [dict_data[cluster_column_name][index]]

    clusters_after_assignment = dict()
    for key, value in to_cluster.items():
        clusters_after_assignment[key] = max(set(value), key=value.count)

    return clusters_after_assignment


def get_repeated_values(dict_data, cluster_column_name, target_column_name):
    clusters_after_assignment = get_assignments(dict_data, cluster_column_name, target_column_name)
    return set(
        [x for x in list(clusters_after_assignment.values()) if list(clusters_after_assignment.values()).count(x) > 1])


def change_text_label_to_number(data, key):
    unique_column = sorted(list(set(data[key])))
    map_text_label = {}
    for index, column in enumerate(unique_column):
        map_text_label[column] = index + 1

    new_key = 'Numeric_{}'.format(key)
    data[new_key] = []
    for column in data[key]:
        data[new_key].append(map_text_label[column])

    return data


def get_count_of_chosen_data(data, key):
    count_table = {}
    for column in data[key]:
        try:
            count_table[column] += 1
        except KeyError:
            count_table[column] = 1

    print(count_table)


def main(args):
    data, headers = read_csv(args[1], rows_count='all')

    dict_data = get_dict_from_data_and_headers(data, headers)
    for key, value in dict_data.items():
        if 'Location' in key or 'Status' in key:
            continue
        # draw_linear(value, key)
        # draw_hist(value, key, filename='{}-img'.format(key))
    locations = list(set(dict_data['Location']))
    # print(locations)
    dict_data = change_text_label_to_number(dict_data, 'Location')
    get_count_of_chosen_data(dict_data, 'Location')
    #
    # data_by_location = {}
    # for location in locations:
    #     data_by_location[location] = []
    #
    # for header, value in dict_data.items():
    #     for index, val in enumerate(value):
    #         if 'Location' in header:
    #             data_by_location[val].append(index)
    #             continue
    #
    # locations_data = {}
    # for location in locations:
    #     locations_data[location] = {}
    #
    # for location, indicies in data_by_location.items():
    #     locations_data[location] = {}
    #     for index in indicies:
    #         for header in headers:
    #             if 'Location' not in header:
    #                 try:
    #                     locations_data[location][header].append(dict_data[header][index])
    #                 except KeyError:
    #                     locations_data[location][header] = [dict_data[header][index]]
    #
    # means = []
    # for location, data in locations_data.items():
    #     data_frame = pandas.DataFrame(data)
    #     means.append((location, data_frame.Price.mean()))
    #     # if len(data_frame) > 50:
    #     #     print(location)
    #     #     print(data_frame.describe())
    #     # print(data_frame.describe())
    #
    # count_estes_per_location = {}
    # for location, data in data_by_location.items():
    #     count_estes_per_location[location] = len(data)
    #
    # plt.figure()
    # sorted_x = sorted(count_estes_per_location.items(), key=operator.itemgetter(1))
    # print(sorted_x)
    # plt.plot([elem[0] for elem in sorted_x], [elem[1] for elem in sorted_x])
    # plt.xticks(rotation=45)
    # plt.xlabel('Location')
    # plt.ylabel('Count')
    # # plt.show()
    #
    #
    # #
    # # print(min(means, key=lambda x: x[1]))
    # # print(max(means, key=lambda x: x[1]))
    # #
    # # print(data_by_location)
    # #
    class_names = list(set(dict_data['Location']))
    # #     print(len(locations_data.keys()))
    data_frame = pandas.DataFrame(dict_data)
    print(data_frame.describe())
    print(data_frame.corr())
    print(data_frame.apply(pandas.Series.nunique))
    #
    # print(data_frame.head())
    #
    km = kmeans.KMeans(data_frame, 5, max_iter=1000, init='k-means++').set_target(
        'Location').evaluate()

    # km.plot_elbow_curve(40)
    dict_data['Cluster'] = km.get_prediction()

    clusters_after_assignment = get_assignments(dict_data, 'Cluster', 'Location')
    correct = 0
    for index, d in enumerate(dict_data['Location']):
        if clusters_after_assignment[d] - dict_data['Cluster'][index] == 0:
            correct += 1

    print(correct / len(dict_data['Location']) * 100)

    crt = cr_tree.ClassificationTree(data_frame, min_samples_leaf=15). \
        set_features(['Size', 'Bedrooms', 'Bathrooms', 'Price']). \
        set_target('Location'). \
        split_data_training_and_test(0.3). \
        train_tree()

    accuracy = crt.get_accuracy()
    crt.tree_visualisation(class_names, filename='/home/sjach/Projects/June2019/dataminging/raport/img/crtree')
    predicted = crt.get_predicted()
    print(accuracy * 100)

    crt.mistake_matrix()

    hierarchical = hierarchical_clusters.HierarchicalClustering(data_frame, n_clusters=5).set_target(
        'Location').evaluate()

    dict_data['Cluster'] = hierarchical.get_prediction()

    clusters_after_assignment = get_assignments(dict_data, 'Cluster', 'Location')
    correct = 0
    for index, d in enumerate(dict_data['Location']):
        if clusters_after_assignment[d] - dict_data['Cluster'][index] == 0:
            correct += 1

    print(correct / len(dict_data['Location']) * 100)
    hierarchical.plot()


if __name__ == "__main__":
    main(sys.argv)
