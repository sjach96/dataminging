import pydotplus
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.tree import export_graphviz
from io import StringIO
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report


class ClassificationTree:

    def __init__(self, data_frame, **kwargs):
        if isinstance(data_frame, dict):
            self._data_frame = pd.DataFrame(data_frame)
        elif isinstance(data_frame, pd.DataFrame):
            self._data_frame = data_frame
        else:
            raise Exception("Unknown type! Expected DataFrame or dict, get: {}".format(type(data_frame)))
        self._features = None
        self._target = None
        self._classifier = DecisionTreeClassifier(**kwargs)
        self._features_train = None
        self._features_test = None
        self._target_train = None
        self._target_test = None
        self._predicted = None
        self._features_values = None

    def set_features(self, features):
        self._features = self._data_frame[features]
        self._features_values = features
        return self

    def set_target(self, target):
        self._target = self._data_frame.__getattr__(target)
        return self

    def split_data_training_and_test(self, test_size):
        self._features_train, self._features_test, self._target_train, self._target_test = train_test_split(
            self._features,
            self._target,
            test_size=test_size,
            random_state=1)
        return self

    def train_tree(self):
        self._classifier = self._classifier.fit(self._features_train, self._target_train)
        self._predicted = self._classifier.predict(self._features_test)
        return self

    def get_predicted(self):
        return self._predicted

    def get_accuracy(self):
        return metrics.accuracy_score(self._target_test, self._predicted)

    def tree_visualisation(self, class_names, filename):
        dot_data = StringIO()
        export_graphviz(self._classifier, out_file=dot_data, filled=True, rounded=True, special_characters=True,
                        feature_names=self._features_values, class_names=class_names)
        graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
        graph.write_svg(filename if '.svg' in filename else '{}.svg'.format(filename))

    def mistake_matrix(self):
        labels = sorted(list(set(list(self._data_frame['Location']))))
        real_values = list(self._data_frame['Location'])[len(self._data_frame['Location']) - 236: -1]
        predicted_values = self._predicted
        print(len(self._predicted))
        conf_matrix = confusion_matrix(real_values, predicted_values, labels=labels)
        for row in conf_matrix:
            for column in row:
                print('{} '.format(column), end='')
            print(' ')

        print(classification_report(real_values, predicted_values, labels=labels))

