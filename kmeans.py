import pandas as pd
import math
import numpy as np
import sklearn.cluster
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import sklearn.neighbors
import sklearn.preprocessing
from sklearn.metrics.pairwise import euclidean_distances


class KMeans:

    def __init__(self, data_frame, clusters, **kwargs):
        if isinstance(data_frame, dict):
            self._data_frame = pd.DataFrame(data_frame)
        elif isinstance(data_frame, pd.DataFrame):
            self._data_frame = data_frame
        else:
            raise Exception("Unknown type! Expected DataFrame or dict, get: {}".format(type(data_frame)))

        self.kmeans = sklearn.cluster.KMeans(n_clusters=clusters, **kwargs)
        self.train = None
        self.target = None

    def set_target(self, target):
        self.target = target
        self.train = np.array(self._data_frame.drop([target], 1))
        return self

    def evaluate(self):
        self.kmeans.fit(self.train)
        return self

    def get_prediction(self):
        cluster = self.kmeans.predict(self._data_frame.drop([self.target], 1))
        return cluster

    def plot_elbow_curve(self, in_range):
        rng = range(1, in_range)
        kmeans_elbow = [sklearn.cluster.KMeans(n_clusters=i) for i in rng]
        score = [kmeans_elbow[i].fit(self.train).score(self.train) for i in range(len(kmeans_elbow))]
        plt.plot(rng, score)
        plt.xlabel('Number of Clusters')
        plt.ylabel('Score')
        plt.title('Elbow Curve')
        plt.show()

    def center_distance(self):
        centroids = self.kmeans.cluster_centers_

        dist = sklearn.neighbors.DistanceMetric.get_metric('euclidean')
        distances = dist.pairwise(centroids)
        distances = euclidean_distances(sklearn.preprocessing.StandardScaler().fit_transform(distances))

        print('\tCluster-1 Cluster-2 Cluster-3 Cluster-4 Cluster-5')
        for index, distance in enumerate(distances):
            print('Cluster-{} '.format(index+1), end='')
            for j, d in enumerate(distance):
                print(round(d, 2), end=' ')
            print(' ')


        # print(centroids)
        #
        # for i in range(dimension):
        #     name_column = "Cluster {}".format(i)
        #     for j in range(dimension):
        #         squre_sum = 0
        #         for center in centroids[j]:
        #             squre_sum += center * center
        #
        #         try:
        #             distances[name_column].append(math.sqrt(squre_sum))
        #         except KeyError:
        #             distances[name_column] = []
        #
        # print(distances)
